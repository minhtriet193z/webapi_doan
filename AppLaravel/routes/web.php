<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



//LOAISANPHAM
Route::get('LoaiSanPham','LoaiSanPhamController@index');

Route::get('LoaiSanPham/{id}','LoaiSanPhamController@show');

//SANPHAM
//Route::resource('SanPham','SanPhamController@index');
Route::get('SanPham', 'SanPhamController@index');

Route::post('SanPham', 'SanPhamController@store');

Route::get('SanPham/{id}', 'SanPhamController@show');

Route::put('SanPham/{id}', 'SanPhamController@update');

Route::delete('SanPham/{id}', 'SanPhamController@destroy');

Route::get('SanPhamTheoLoai/{id}', 'SanPhamController@showtheoloai');

Route::get('SanPhamTheoUser/{id}', 'SanPhamController@showtheouser');

Route::get('SanPhamSearch/{ten}', 'SanPhamController@search');


//USER
Route::get('User', 'UserController@index');

Route::post('User', 'UserController@store');

Route::get('User/{id}', 'UserController@show');

Route::put('User/{id}', 'UserController@update');

Route::delete('User/{id}', 'UserController@destroy');

Route::get('UserTheoFacebookId/{id}', 'UserController@showtheofacebookid');

Route::get('UserTheoEmail/{email}', 'UserController@showtheoemail');

Route::get('UserShowFriend/{id}', 'UserController@showfriend');

Route::get('UserShowGroup/{id}', 'UserController@showgroup');

Route::get('UserShowFavorite/{id}', 'UserController@showfavorite');

Route::get('UserShowSanPham/{id}', 'UserController@showsanpham');


Route::post('UserSendMessager', 'UserController@sendMessager');


//FRIEND
Route::post('Friend', 'FriendController@store');

Route::get('Friend/{id}', 'FriendController@show');

Route::get('Friend/{user_id}/{friend_id}', 'FriendController@search');

Route::delete('Friend/{id}', 'FriendController@destroy');

Route::get('FriendXoa/{user_id}/{friend_id}', 'FriendController@xoa');



//FAVORITE
Route::post('Favorite', 'FavoriteController@store');

Route::get('Favorite/{id}', 'FavoriteController@show');

Route::get('Favorite/{user_id}/{sanpham_id}', 'FavoriteController@search');

Route::get('FavoriteXoa/{user_id}/{sanpham_id}','FavoriteController@xoa');


//GROUP
Route::post('Group', 'GroupController@store');

Route::get('Group/{id}', 'GroupController@show');

Route::get('Group/{send_id}/{receive_id}', 'GroupController@find');

Route::delete('Group/{id}', 'GroupController@destroy');
