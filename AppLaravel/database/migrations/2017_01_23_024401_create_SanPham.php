<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSanPham extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        schema::create("sanpham", function(Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->integer("gia");
            $table->longtext("hinh");
            $table->text('diachi');
            $table->longtext('chitiet');
            $table->string('tinhtrang');
            $table->integer("view");
            $table->integer("loai_id")->unsigned();
            $table->foreign('loai_id')->references('id')->on('loaisanpham')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        schema::dropIfExists("sanpham");
    }
}
