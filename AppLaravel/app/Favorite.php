<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    //
    protected $table = "favorite";
    public $timestamps = false;
    protected $fillable = [
    	'user_id', 'sanpham_id'
    ];

    public function user() {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function sanpham() {
    	return $this->belongsTo('App\SanPham', 'sanpham_id', 'id');
    }
}
