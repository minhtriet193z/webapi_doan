<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'chat_id', 'facebook_id', 'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sanpham() {
        return $this->hasMany('App\SanPham', 'user_id', 'id');
    }

    public function favorite() {
        return $this->hasMany('App\Favorite', 'user_id', 'id');
    }


    public function friend() {
        return $this->hasMany('App\Friend', 'user_id', 'id');
    }

    public function groups() {
        return $this->hasMany('App\Group', 'user_send_id', 'id');
    }
}
