<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Friend;

class FriendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $friend = new Friend();
        $friend->user_id = $request->user_id;
        $friend->friend_id = $request->friend_id;
        $friend->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $friend = Friend::find($id);
        return $friend;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Friend::destroy($id);
    }

    public function search($user_id, $friend_id)
    {
        $friend = Friend::where('user_id',$user_id)->where("friend_id",$friend_id)->get();
        return $friend;
    }

    public function xoa($user_id, $friend_id){
        $friend = Friend::where('user_id',$user_id)->where("friend_id",$friend_id)->first();
        $friend->delete();

        $friend = Friend::where('user_id',$friend_id)->where("friend_id",$user_id)->first();
        $friend->delete();
    }
}
