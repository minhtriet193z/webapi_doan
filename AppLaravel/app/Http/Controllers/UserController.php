<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Friend;
use App\Group;
use DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = User::all();
        return $user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->facebook_id = $request->facebook_id;
        $user->chat_id = $request->chat_id;
        $user->phone = $request->phone;

        $user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::find($id);
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->facebook_id = $request->facebook_id;
        $user->chat_id = $request->chat_id;
        $user->phone = $request->phone;

        $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        user::destroy($id);
    }

    public function showtheofacebookid($id){
        $user = User::where('facebook_id',$id)->get();
        return $user;
    }

    public function showtheoemail($email){
        $user = User::where('email',$email)->get();
        return $user;
    }

    public function showfriend($id){
        /*
        $user = User::find($id);
        $friend = $user->friend()->get();
        return $friend;
        */
        $user = DB::table("friends")->where("user_id",$id)->join("users",function($join){
            $join->on("friends.friend_id", "=", "users.id");
        })->get();
        return $user;
    }

    public function showgroup($id){
        $user = User::find($id);
        $group = Group::where('user_send_id',$id)->orWhere('user_receive_id',$id)->get();
        return $group;
    }

    public function showfavorite($id){
        //$user = User::find($id);
        //$favorite = $user->favorite()->sanpham()->get();
        //return $favorite;
        /*
        $sanpham = DB::table("sanpham")->join('favorite', function($join){
            $join->on('sanpham.id', '=', 'favorite.sanpham_id')
                 ->where('favorite.user_id','=',15);
        })->get();
        return $sanpham;
        */

        $favorite = DB::table("favorite")->where('favorite.user_id',$id)->join('sanpham', function($join){
                $join->on('favorite.sanpham_id', '=', 'sanpham.id');      
        })->get();

        return $favorite;
    }

    public function showsanpham($id){
        $user = User::find($id);
        $sanpham = $user->sanpham()->get();
        return $sanpham;
    }


    public function sendMessager(Request $request){

        //$message = 'abc';
        //$title = 'cba';
        
        $message = $request->message;
        $title = $request->title;
        $user_receive_token = $request->user_receive_token;

        $patch_to_fcm = "https://fcm.googleapis.com/fcm/send";

        $server_key = 'AAAAlkz7Obw:APA91bHX9B0N6lj22pb3hyRUB1vmWT2Z7o6t3zGW9rtMJ7ARXlIxsoeTlrd0cyopih0i-d3Bu9ycTpjuXs27_aH-PyyauNERby8r_STXpalaK4HsTM3GgwQYx7QXB7SmH7rNCWLLcDMb';

        $key = $user_receive_token;

        $headers = array(
                    'Authorization:key='.$server_key,
                    'Content-Type:application/json'
            );


        $fields = array('to'=>$key,'notification'=>array('title'=>$title,'body'=>$message));

        $payload = json_encode($fields);

        $curl_session = curl_init();

        curl_setopt($curl_session, CURLOPT_URL, $patch_to_fcm);
        curl_setopt($curl_session, CURLOPT_POST, true);
        curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_session, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $payload);

        $result = curl_exec($curl_session);

        curl_close($curl_session);

        return 'da gui thanh cong';
    }
}
