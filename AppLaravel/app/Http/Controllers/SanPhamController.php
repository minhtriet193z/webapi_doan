<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SanPham;
class SanPhamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sanpham = SanPham::all();
        return $sanpham;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $sp = new SanPham();
        $sp->name = $request->name;
        $sp->gia = $request->gia;
        $sp->hinh = $request->hinh;
        $sp->diachi = $request->diachi;
        $sp->chitiet = $request->chitiet;
        $sp->tinhtrang = $request->tinhtrang;
        $sp->view = $request->view;
        $sp->loai_id = $request->loai_id;
        $sp->user_id = $request->user_id;

        $sp->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $sanpham = SanPham::find($id);
        return $sanpham;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sp = SanPham::findOrFail($id);

        $sp->name = $request->name;
        $sp->gia = $request->gia;
        $sp->hinh = $request->hinh;
        $sp->diachi = $request->diachi;
        $sp->chitiet = $request->chitiet;
        $sp->tinhtrang = $request->tinhtrang;
        $sp->view = $request->view;
        $sp->loai_id = $request->loai_id;
        $sp->user_id = $request->user_id;

        $sp->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SanPham::destroy($id);
    }

    public function showtheoloai($loai_id){
        $sanpham = SanPham::where("loai_id",$loai_id)->get();
        return $sanpham;
    }

    public function showtheouser($user_id){
        $sanpham = SanPham::where("user_id",$user_id)->get();
        return $sanpham;
    }

    public function search($ten){
        $sanpham = SanPham::where("name","like","%". $ten ."%")->get();
        return $sanpham;
    }
}
