<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoaiSanPham;
use App\SanPham;
class LoaiSanPhamController extends Controller
{
    //
    public function index(){
        /*
    	$loai = LoaiSanPham::find($id);
    	echo $loai->name;


    	$sp = $loai->sanpham()->get();

    	foreach ($sp as $s) {
    		echo $s->name;
    		# code...
    	}
        */

        $loai = LoaiSanPham::all();
        return $loai;
    }

    public function show($id){
        $loai = LoaiSanPham::find($id);
        return $loai;
    }
}
