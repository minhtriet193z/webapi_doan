<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favorite;

class FavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $favorite = new Favorite();
        $favorite->user_id = $request->user_id;
        $favorite->sanpham_id = $request->sanpham_id;
        $favorite->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $favorite = Favorite::find($id);
        return $favorite;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id, $sanpham_id)
    {
        //
        $favorite = Favorite::where("user_id",$user_id)->where("sanpham_id",$sanpham_id)->get();
        $favorite->destroy();
    }

    public function search($user_id, $sanpham_id)
    {
        //
        $favorite = Favorite::where("user_id",$user_id)->where("sanpham_id",$sanpham_id)->get();
        return $favorite;
    }

    public function xoa($user_id, $sanpham_id)
    {
        //
        $favorite = Favorite::where("user_id",$user_id)->where("sanpham_id",$sanpham_id)->first();
        $favorite->delete();
    }
}
