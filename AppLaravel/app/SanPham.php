<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SanPham extends Model
{
    //
    protected $table = 'sanpham';

    protected $fillable = [
    	'id', 'name', 'gia', 'hinh', 'loai_id', 'tinhtrang', 'chitiet', 'user_id', 'diachi'
    ];

    //public $timestamps = false;

    public function loaisanpham() {
    	return $this->belongsTo('App\LoaiSanPham', 'loai_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function favorite() {
    	return $this->hasMany('App\Favorite', 'sanpham_id', 'id');
    }

    public function images() {
        return $this->hasMany('App\SanPhamImage', 'sanpham_id', 'id');
    }
}
