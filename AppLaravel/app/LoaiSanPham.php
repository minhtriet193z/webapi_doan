<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiSanPham extends Model
{
    //
    protected $table = 'loaisanpham';
	public $timestamps = false;

    protected $fillable = [
    	'id', 'name', 'alias', 'description', 'hinh'
    ];

    public function sanpham(){
    	return $this->hasMany('App\SanPham', 'loai_id','id');
    }
}
