<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    protected $table = "groups";
    public $timestamps = false;
    protected $fillable = [
    	'id' ,'user_send_id', 'user_receive_id'
    ];

    public function user() {
    	return $this->belongsTo('App\User', 'user_send_id', 'id');
    }
}
