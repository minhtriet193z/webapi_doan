<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SanPhamImage extends Model
{
    //
    protected $table = 'sanpham_images';

    protected $fillable = [
    	'sanpham_id', 'hinh'
    ];

    public $timestamps = false;

    public function sanpham() {
    	return $this->belongsTo('App\SanPham','sanpham_id','id');
    }
}
